import os
import sys
import binascii

if (len(sys.argv) < 2):
	print("You must drag and drop a file on bin2array.py for it to convert it");
	while (True):
		wait = 1;

binFilePath = os.path.abspath(sys.argv[1]);
print("Bin File: \"" + binFilePath + "\"");

baseFileName, extension = os.path.splitext(binFilePath);
outputFilePath = baseFileName + ".c"

rowSize = 16
pageSize = 512

print("Writing to \"" + outputFilePath + "\"")

fileLength = os.path.getsize(binFilePath);
file = open(binFilePath, "rb");
print("File is " + str(fileLength) + " bytes long")
outputFile = open(outputFilePath, "w")
outputFile.write("uint8_t array[] = {\n\n//Page 0\n/*0:0x00000000*/")
try:
	index = 0
	newByte = file.read(1)
	while (index < fileLength):
		if (index != 0):
			pageStart = index - (index%pageSize);
			if (((index-pageStart)%rowSize) == 0 or (index%pageSize) == 0): 
				outputFile.write(",\n");
				if ((index%pageSize) == 0):
					outputFile.write("\n//Page " + str(int(index/pageSize)) + "\n");
				outputFile.write("/*" + str(int((index-pageStart)/rowSize)) + ":0x" + (hex(index)[2:]).zfill(8) + "*/");
			else: outputFile.write(", ");
			if ():
				outputFile.write("")
		
		outputFile.write("0x" + newByte.hex());
		
		newByte = file.read(1);
		index += 1
		
		currentPercent = int(index/fileLength*100);
		lastPercent = int((index-1)/fileLength*100);
		if (index != 0 and currentPercent != lastPercent and (currentPercent%10) == 0):
			print(str(int((index/fileLength)*100)) + "%");
finally:
	file.close();

outputFile.write("\n}; // " + str(index) + " bytes long\n")

outputFile.close();

print("Done!");

# while (True):
# 	wait = 1;